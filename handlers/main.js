/**
 * Модуль, собирающий все обработчики
 */
var auth = require('./auth');
var user = require('./user');
var users = require('./users');
var friendships = require('./friendships');
var friendship = require('./friendship');

module.exports = {
  auth: auth,
  user: user,
  users: users,
  friendships: friendships,
  friendship: friendship,
};